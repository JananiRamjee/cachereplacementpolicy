module set(clk,
           rst,
           entry,
           o_tag,
           writedata,
           byte_en,
           write,
           word_en,
           r_cm_d,
           readdata,
		  
		ctrx,
           wb_addr,
           hit,
           modify,
           miss,
           valid,
           read_miss,
		   avail,
		   allinv,
		   counter);

    parameter cache_entry = 14;

    input wire                    clk, rst;
    input wire [cache_entry-1:0]  entry;
    input wire [22-cache_entry:0] o_tag;
    input wire [127:0] 		      writedata;
    input wire [3:0] 		      byte_en;
    input wire       	          write;
    input wire [3:0]              word_en;
    input wire 			          read_miss;
	input wire [7:0]     r_cm_d;

    output wire [127:0] 		  readdata;
    output wire [22:0] 		      wb_addr;
    output wire 			      hit, modify, miss, valid;
	wire [2:0]				  cnt;
    wire av,ivt,cx;
    
    output wire[15:0] avail;//c
    output wire  [47:0] counter;//c
    wire [15:0] inv;//c
	output wire [3:0] allinv;
	output wire [15:0] ctrx;   

    wire [22-cache_entry:0] 	 i_tag;
    wire 			             dirty;
    wire [24-cache_entry:0] 	 write_tag_data;
	//output wire [15:0] ft;
	//reg nothit;
	

    assign hit = valid && (o_tag == i_tag);
    assign modify = valid && (o_tag != i_tag) && dirty;
    assign miss = !valid || ((o_tag != i_tag) && !dirty);
	
 
    assign wb_addr = {i_tag, entry};
	

    //write -> [3:0] write, writedata/readdata 32bit -> 128bit

    s_ram #(.width(8), .widthad(cache_entry)) ram11_3(clk, entry, write && word_en[3]  && byte_en[3], writedata[127:120], entry, readdata[127:120], counter[47:45], avail[15], inv[15],ctrx[15]);
	hamcalinv r11_3(inv[15],readdata[127:120],r_cm_d,valid,hit);    
	s_ram #(.width(8), .widthad(cache_entry)) ram11_2(clk, entry, write && word_en[3]  && byte_en[2], writedata[119:112], entry, readdata[119:112], counter[44:42], avail[14], inv[14],ctrx[14]);
	hamcalinv r11_2(inv[14],readdata[119:112],r_cm_d,valid,hit);
    s_ram #(.width(8), .widthad(cache_entry)) ram11_1(clk, entry, write && word_en[3]  && byte_en[1], writedata[111:104], entry, readdata[111:104], counter[41:39], avail[13], inv[13],ctrx[13]);
	hamcalinv r11_1(inv[13],readdata[111:104],r_cm_d,valid,hit);
    s_ram #(.width(8), .widthad(cache_entry)) ram11_0(clk, entry, write && word_en[3]  && byte_en[0], writedata[103:96], entry, readdata[103:96], counter[38:36], avail[12], inv[12],ctrx[12]);
	hamcalinv r11_0(inv[12],readdata[103:96],r_cm_d,valid,hit);
	allinvert r11(allinv[3],inv[15:12]);

    s_ram #(.width(8), .widthad(cache_entry)) ram10_3(clk, entry, write && word_en[2]  && byte_en[3], writedata[95:88], entry, readdata[95:88], counter[35:33], avail[11], inv[11],ctrx[11]);
	hamcalinv r10_3(inv[11],readdata[95:88],r_cm_d,valid,hit);
    s_ram #(.width(8), .widthad(cache_entry)) ram10_2(clk, entry, write && word_en[2]  && byte_en[2], writedata[87:80], entry, readdata[87:80], counter[32:30], avail[10], inv[10],ctrx[10]);
	hamcalinv r10_2(inv[10],readdata[87:80],r_cm_d,valid,hit);
    s_ram #(.width(8), .widthad(cache_entry)) ram10_1(clk, entry, write && word_en[2]  && byte_en[1], writedata[79:72], entry, readdata[79:72], counter[29:27], avail[9], inv[9],ctrx[9]);
	hamcalinv r10_1(inv[9],readdata[79:72],r_cm_d,valid,hit);
    s_ram #(.width(8), .widthad(cache_entry)) ram10_0(clk, entry, write && word_en[2]  && byte_en[0], writedata[71:64], entry, readdata[71:64], counter[26:24], avail[8], inv[8],ctrx[8]);
	hamcalinv r10_0(inv[8],readdata[71:64],r_cm_d,valid,hit);
	allinvert r10(allinv[2], inv[11:8]);

    s_ram #(.width(8), .widthad(cache_entry)) ram01_3(clk, entry, write && word_en[1]  && byte_en[3], writedata[63:56], entry, readdata[63:56], counter[23:21], avail[7], inv[7],ctrx[7]);
	hamcalinv r01_3(inv[7],readdata[63:56],r_cm_d,valid,hit);
    s_ram #(.width(8), .widthad(cache_entry)) ram01_2(clk, entry, write && word_en[1]  && byte_en[2], writedata[55:48], entry, readdata[55:48], counter[20:18], avail[6], inv[6],ctrx[6]);
	hamcalinv r01_2(inv[6],readdata[55:48],r_cm_d,valid,hit);
    s_ram #(.width(8), .widthad(cache_entry)) ram01_1(clk, entry, write && word_en[1]  && byte_en[1], writedata[47:40], entry, readdata[47:40], counter[17:15], avail[5], inv[5],ctrx[5]);
	hamcalinv r01_1(inv[5],readdata[47:40],r_cm_d,valid,hit);
    s_ram #(.width(8), .widthad(cache_entry)) ram01_0(clk, entry, write && word_en[1]  && byte_en[0], writedata[39:32], entry, readdata[39:32], counter[14:12], avail[4], inv[4],ctrx[4]);
	hamcalinv r01_0(inv[4],readdata[39:32],r_cm_d,valid,hit);
	allinvert r01(allinv[1], inv[7:4]);

    s_ram #(.width(8), .widthad(cache_entry)) ram00_3(clk, entry, write && word_en[0]  && byte_en[3], writedata[31:24], entry, readdata[31:24], counter[11:9], avail[3], inv[3],ctrx[3]);
	hamcalinv r00_3(inv[3],readdata[31:24],r_cm_d,valid,hit);
    s_ram #(.width(8), .widthad(cache_entry)) ram00_2(clk, entry, write && word_en[0]  && byte_en[2], writedata[23:16], entry, readdata[23:16], counter[8:6], avail[2], inv[2],ctrx[2]);
	hamcalinv r00_2(inv[2],readdata[23:16],r_cm_d,valid,hit);
    s_ram #(.width(8), .widthad(cache_entry)) ram00_1(clk, entry, write && word_en[0]  && byte_en[1], writedata[15: 8], entry, readdata[15:8], counter[5:3], avail[1], inv[1],ctrx[1]);
	hamcalinv r00_1(inv[1],readdata[15:8],r_cm_d,valid,hit);
    s_ram #(.width(8), .widthad(cache_entry)) ram00_0(clk, entry, write && word_en[0]  && byte_en[0], writedata[ 7: 0], entry, readdata[ 7:0], counter[2:0], avail[0], inv[0],ctrx[0]);
	hamcalinv r00_0(inv[0],readdata[7:0],r_cm_d,valid,hit);
	allinvert r00(allinv[0], inv[3:0]);
	
    assign write_tag_data = (read_miss) ? {1'b0, 1'b1, o_tag} : (modify || miss ) ? {1'b1, 1'b1, o_tag} : {1'b1, 1'b1, i_tag};
    s_ram #(.width(25-cache_entry), .widthad(cache_entry)) ram_tag(clk, entry, write, write_tag_data, entry, {dirty, valid, i_tag}, cnt, av, ivt,cx);

`ifdef SIM
    integer i;

    initial begin
        for(i = 0; i <=(2**cache_entry-1); i=i+1) begin
	        ram_tag.mem[i] = 0;
        end
    end
`endif
endmodule
