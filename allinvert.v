module allinvert(alin,
			   in);
output reg alin;
input wire [3:0] in;
always@(*)
begin
if(in == 4'b0000)
 alin = 1'b0;
else
 alin = 1'b1;
end
endmodule

