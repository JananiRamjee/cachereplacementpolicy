module hamcalinv( inv,
                  i_p_data,
		  		  setdata,
		  		  valid,hit1);
input wire [7:0] i_p_data, setdata;
output reg inv;
input wire valid,hit1;
integer i;
integer h=0;
always@(*)
if(!hit1)
begin
 begin
  for(i=0; i <= 7; i=i+1)
   begin
    if(i_p_data[i] != setdata[i])
     h = h+1;
    end
if (h > 5)
begin
 inv =1'b1;
 end
else
begin
 inv =1'b0;
end
 end
end
endmodule
