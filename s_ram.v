module s_ram
  #(parameter width     = 1,
    parameter widthad   = 1
    )
   (
    input 		   clk,
    
    input [widthad-1:0]    wraddress,
    input 		   wren,
    input [width-1:0] 	   data,
    
    input [widthad-1:0]    rdaddress,
    output reg [width-1:0] q,
    output reg [2:0] cter,
    output  reg avail,
    output  reg inv,
    output reg cterx
    );

reg [width-1:0] mem [(2**widthad)-1:0];
integer k;

always @(posedge clk) 
begin
    if(wren)
begin
if (! inv)
 mem[wraddress] <= data;
else
begin
for (k=0; k<=7; k=k+1)
mem[k] <= ~data[k];
end
   cter <= cter + 3'b001;
   if(cter==3'b111)
    cterx<=1'b1;
 end    
else
begin
if(cterx)
cter<=cter-1;
end
 if (cter == 3'b000)
begin   
avail<=1'b1;
cterx<=1'b0;
end
 else 
 avail<=1'b0;
    q <= mem[rdaddress];
end

endmodule

