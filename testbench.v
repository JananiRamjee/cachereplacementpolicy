module testbench;

reg         clk1, rst1;
reg [24:0]  i_p_addr1;
reg [3:0]   i_p_byte_en1;
reg [31:0]  i_p_writedata1;
reg         i_p_read1, i_p_write1;
wire [31:0]  o_p_readdata1;
wire         o_p_readdata_valid1;
wire        o_p_waitrequest1;
wire [25:0]  o_m_addr1;
wire [3:0]  o_m_byte_en1;
wire [127:0] o_m_writedata1;
wire         o_m_read1, o_m_write1;
wire [191:0]  o_counter1;
wire [63:0]  o_avail1;
wire [15:0]  o_inv1;
wire [63:0] o_ctx1;

    
reg [127:0] i_m_readdata1;
reg         i_m_readdata_valid1;
reg         i_m_waitrequest1;

wire [31:0]  cnt_r1;
wire [31:0]  cnt_w1;
wire [31:0]  cnt_hit_r1;
wire [31:0]  cnt_hit_w1;
wire [31:0]  cnt_wb_r1;
wire [31:0]  cnt_wb_w1;

always
#5 clk1 = ~clk1;
cache c1(clk1,
               rst1,
               i_p_addr1,
               i_p_byte_en1,
               i_p_writedata1,
               i_p_read1,
               i_p_write1,
               o_p_readdata1,
               o_p_readdata_valid1,
               o_p_waitrequest1,

               o_m_addr1,
               o_m_byte_en1,
               o_m_writedata1,
               o_m_read1,
               o_m_write1,
			   o_ctx1,
			   o_counter1,
			   o_avail1,
			   o_inv1,

               i_m_readdata1,
               i_m_readdata_valid1,
               i_m_waitrequest1,

               cnt_r1,
               cnt_w1,
               cnt_hit_r1,
               cnt_hit_w1,
               cnt_wb_r1,
               cnt_wb_w1);
initial 
 begin
 clk1=1'b1;
  rst1=1'b1;
  i_p_addr1=25'b0000000000000000000000001;
  i_p_byte_en1=4'b0001;
  i_p_writedata1=32'b00000000000000000000000000000001;
 i_p_read1=1'b1;
  i_p_write1=1'b0;
  
   i_m_readdata1=128'b010;
  i_m_readdata_valid1=1'b1;
  i_m_waitrequest1=1'b1;
  
  $monitor($time,"o_p_readdata_valid1=%b",o_p_readdata_valid1);
  #25 rst1=1'b0;
  #500 $finish;
  end
  endmodule
